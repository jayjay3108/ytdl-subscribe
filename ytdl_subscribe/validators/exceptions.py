class ValidationException(ValueError):
    """Any user-caused configuration error should result in this error"""
